//services
var services = angular.module('netchat.service', []);

services.service("ChatService", function ($q, $timeout) {
    var service = {},
        listener = $q.defer(),
        deleteListener = $q.defer(),
        loadInitListener = $q.defer(),
        socket = {
            client: null,
            stomp: null
        };
    var msgSubscr, delSubscr, initSubscr;
    var isInitialized = false;

    service.RECONNECT_TIMEOUT = 10000;
    service.SOCKET_URL = BASE_PATH + "chat";
    service.CHAT_TOPIC = "/topic/message";
    service.DELETE_TOPIC = "/topic/delete";
    service.INIT = "/user/topic/initmessages"
    service.CHAT_BROKER = "/app/newmessage";
    service.DELETE_BROKER = "/app/delete";
    service.INIT_BROKER = "/app/loadinit";

    service.delete = function () {
        return deleteListener.promise;
    };

    service.init = function () {
        return loadInitListener.promise;
    };

    service.receive = function () {
        return listener.promise;
    };

    service.doDelete = function (message) {
        socket.stomp.send(service.DELETE_BROKER, {
            priority: 9
        }, JSON.stringify(message));
    }

    service.doInit = function () {
        socket.stomp.send(service.INIT_BROKER, {priority: 9}, null);
    }

    service.send = function (content) {
        socket.stomp.send(service.CHAT_BROKER, {
            priority: 9
        }, JSON.stringify({
            content: content,
            id: null
        }));
    };

    var reconnect = function () {
        console.log("connection closed. I will try to reconnect");
        $timeout(function () {
            service.unsubscribe();
            service.initialize();
        }, service.RECONNECT_TIMEOUT);
    };

    var getMessage = function (data) {
        console.log(data);
        var message = JSON.parse(data), out = {};
        out.id = message.id;
        out.content = message.content;
        out.creationDate = new Date(message.creationDate);
        out.user = message.user;
        return out;
    };

    var getMessages = function (data) {
        var messages = JSON.parse(data), out = [];
        messages.forEach(function (message) {
            var msg = {};
            msg.id = message.id;
            msg.content = message.content;
            msg.creationDate = new Date(message.creationDate);
            msg.user = message.user;
            out.push(msg);
        })
        return out;
    };

    var startListeners = function () {
        //new message
        msgSubscr = socket.stomp.subscribe(service.CHAT_TOPIC, function (data) {
            listener.notify(getMessage(data.body));
        });

        //delete message
        delSubscr = socket.stomp.subscribe(service.DELETE_TOPIC, function (data) {
            deleteListener.notify(getMessage(data.body));
        });

        //load initial messages
        initSubscr = socket.stomp.subscribe(service.INIT, function (data) {
            loadInitListener.notify(getMessages(data.body));
        });

        if (!isInitialized) {//do not init if reconnect
            isInitialized = true;
            //send request to load initial messages
            service.doInit();
        }
    };

    service.initialize = function () {
        socket.client = new SockJS(service.SOCKET_URL);
        socket.stomp = Stomp.over(socket.client);
        socket.stomp.connect({}, startListeners);
        socket.client.onclose = function () {
            reconnect();
        };
    };

    service.cleanup = function () {
        isInitialized = false;
        listener = $q.defer(),
            deleteListener = $q.defer(),
            loadInitListener = $q.defer();
        service.unsubscribe();
    }

    service.unsubscribe=function () {
        msgSubscr.unsubscribe();
        delSubscr.unsubscribe();
        initSubscr.unsubscribe();
        socket.stomp.disconnect(function () {
            socket.client.close();
            socket = {
                client: null,
                stomp: null
            };
            console.log("disconnected");
        });
    };

    return service;
});
