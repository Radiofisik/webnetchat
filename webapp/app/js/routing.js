var app = angular.module('netchat.routing',[]);
app.config(function ($routeProvider) {
    $routeProvider
        .when('/chat', {templateUrl: "app/html/chat.html", controller: 'chat'})
        .when('/login', {templateUrl: "app/html/login.html", controller: 'login'})
        .when('/logout', {templateUrl: "app/html/login.html", controller: 'login'})
        .otherwise({redirectTo: '/login'});
});
	