var app = angular.module('netchat.chatcontroller', []);

app.controller('chat', ['$rootScope', '$scope', '$location', 'ChatService', function ($rootScope, $scope, $location, ChatService) {
    if(!$rootScope.loggedin){ $location.path('/login'); return;}
    var MAX_MESSAGES = 1000;
    $scope.messages = [];

    $scope.sendMsg = function () {
        ChatService.send($scope.message);
        $scope.message = "";
    }

    $scope.delMsg = function (message) {
        ChatService.doDelete(message);
    }

    ChatService.receive().then(null, null, function (message) {
        console.log(message);
        $scope.messages.push(message);
        if ($scope.messages.length > MAX_MESSAGES) {
            $scope.messages = $scope.messages.slice($scope.messages.length - MAX_MESSAGES);
        }
    });

    ChatService.init().then(null, null, function (messages) {
        $scope.messages = messages;
        console.log(messages);
    });

    ChatService.delete().then(null, null, function (message) {
        var indexToDelete = _.findIndex($scope.messages, function (o) {
            return o.id == message.id;
        });
        if (indexToDelete != -1) {
            $scope.messages[indexToDelete].content = "сообщение удалено пользователем";
            $scope.messages[indexToDelete].style = "deleted";
        }
    });
}]);
	
	