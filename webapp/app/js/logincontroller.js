var app = angular.module('netchat.logincontroller',[]);

app.controller('login', ['$rootScope', '$scope', '$location', '$httpParamSerializerJQLike', '$http', 'ChatService', function ($rootScope, $scope, $location, $httpParamSerializerJQLike, $http, ChatService) {
    $scope.err='';
    $scope.username = '';

    $scope.login = function () {
        var req = {
            method: 'POST',
            url: './j_spring_security_check',
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $httpParamSerializerJQLike({user: $scope.username, pass: 'password'}),
        };
        $http(req).then(
            function (response) {
                console.log(response);

                        if (response.data.success) {
                            $rootScope.loggedin = true;
                            $rootScope.currentuser = $scope.username;
                            $location.path('/chat');
                            ChatService.initialize();
                        }
                        else {
                            $scope.err = "Войти не удалось";
                        }
            },
            function (response) {
                $scope.err = "Проверьте сетевое подключение";
            });
        $location.path('/login');
    }

    $scope.logout = function () {
        ChatService.cleanup();
        $http.post('./logout', {}).finally(function() {
            var req = {
                method: 'POST',
                url: './logout',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            };
            $http(req).then(
                function (response) {
                    console.log(response);

                    if (response.data.success) {
                        $rootScope.loggedin = false;
                        $location.path('/');
                        $rootScope.currentuser = '';
                    }
                    else {
                        $scope.err = "Выйти не удалось";
                    }
                },
                function (response) {
                    $scope.err = "Проверьте сетевое подключение";
                });
        });
    }
}]);
	
	