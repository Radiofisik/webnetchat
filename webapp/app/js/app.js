var app = angular.module('netchat', ['ngRoute',
    'luegg.directives',
    'netchat.service',
    'netchat.chatcontroller',
    'netchat.routing',
    'netchat.logincontroller']);
