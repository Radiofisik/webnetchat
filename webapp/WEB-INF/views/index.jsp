<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en" ng-app="netchat">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>NetChat</title>

    <link href="libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="app/css/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<div ng-include="'app/layout/header.html'"></div>
<div ng-view></div>
<div ng-include="'app/layout/footer.html'"></div>

<script>var BASE_PATH="<c:url value='/'/>";</script>
<script src="libs/jquery/dist/jquery.min.js"></script>
<script src="libs/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="libs/sockjs/sockjs.js" type="text/javascript"></script>
<script src="libs/stomp-websocket/lib/stomp.js" type="text/javascript"></script>
<script src="libs/angular/angular.js"></script>
<script src="libs/angular-route/angular-route.js"></script>
<script src="libs/angular-scroll-glue/src/scrollglue.js"></script>
<script src="libs/lodash/dist/lodash.js"></script>

<script src="app/js/app.js"></script>
<script src="app/js/routing.js"></script>
<script src="app/js/service.js"></script>
<script src="app/js/logincontroller.js"></script>
<script src="app/js/chatcontroller.js"></script>
</body>
</html>
