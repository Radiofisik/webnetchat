package netchat.event;

import netchat.model.Impl.User;
import netchat.model.dao.IUserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;
import org.slf4j.*;
import org.springframework.web.socket.messaging.SessionSubscribeEvent;
import org.springframework.web.socket.messaging.SessionUnsubscribeEvent;

@Component
public class SessionEventListener {
    @Autowired
    IUserDao userDao;

    private Logger logger = LoggerFactory.getLogger(getClass());

    @EventListener
    private void userConnected(SessionConnectEvent event){
        SimpMessageHeaderAccessor headers = SimpMessageHeaderAccessor.wrap(event.getMessage());
        String username = headers.getUser().getName();
        if(userDao.getByLogin(username)==null) {
            User user = new User();
            user.setLogin(username);
            userDao.save(user);
        }
        logger.info("user connected "+username);
    }

}
