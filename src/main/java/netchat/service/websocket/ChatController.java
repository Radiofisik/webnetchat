package netchat.service.websocket;

import netchat.model.Impl.Message;
import netchat.model.dao.IMessageDao;
import netchat.model.dao.IUserDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;

import java.security.Principal;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Controller
public class ChatController {
    private final int NUMBER_OF_INITIAL_MESSAGES=20;

    @Autowired
    IUserDao userDao;

    @Autowired
    IMessageDao messageDao;

    private Logger logger = LoggerFactory.getLogger(getClass());

    @MessageMapping("/newmessage")
    @SendTo(value = "/topic/message")
    public Message sendMessage(Message message, Principal principal) {
        message.setCreationDate(new Date());
        String name = principal.getName(); //get logged in username
        message.setUser(userDao.getByLogin(name));
        messageDao.save(message);
        return message;
    }

    @MessageMapping("/loadinit")
    @SendToUser("/topic/initmessages")
    public Collection<Message> getLastMessages(Principal principal) {
        logger.info("init messages has been sent to user "+principal.getName());
        return messageDao.getLastMessages(NUMBER_OF_INITIAL_MESSAGES);
    }

    @MessageMapping("/delete")
    @SendTo(value = "/topic/delete")
    public Message deleteMessage(Message message, Principal principal) {
        if (principal.getName().equals(message.getUser().getLogin())) {
            messageDao.delete(message);
            logger.info("message "+ message + "has been deleted by user "+ principal.getName());
            return message;
        }
        else return null;
    }
}
