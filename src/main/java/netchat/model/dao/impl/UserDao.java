package netchat.model.dao.impl;

import netchat.model.Impl.User;
import netchat.model.dao.IUserDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class UserDao implements IUserDao {

    private Logger logger = LoggerFactory.getLogger(getClass());

    private Set<User> users = new HashSet<>();
    private int nextId = 0;

    @Override
    public User get(Integer id) {
        return users.stream().filter((x) -> x.getId().equals(id)).findFirst().get();
    }

    @Override
    public Collection<User> getAll() {
        return users;
    }

    @Override
    public void save(User object) {
        if (object.getId() == null) object.setId(nextId++);
        users.add(object);
    }

    @Override
    public void delete(User object) {
        users.remove(object);
    }

    @Override
    public User getByLogin(String login) {
        try {
            return users.stream().filter((x) -> x.getLogin().equals(login)).findFirst().get();
        } catch (NoSuchElementException e) {
            logger.info("user " + login + " not found");
            return null;
        }
    }
}
