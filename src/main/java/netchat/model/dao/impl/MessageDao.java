package netchat.model.dao.impl;

import netchat.model.Impl.Message;
import netchat.model.dao.IMessageDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class MessageDao implements IMessageDao {

    private Logger logger = LoggerFactory.getLogger(getClass());

    private ArrayList<Message> messages=new ArrayList<>();
    private static int nextId=0;

    @Override
    public Message get(Integer id) {
        return messages.stream().filter((x)->x.getId().equals(id)).findFirst().get();
    }

    @Override
    public Collection<Message> getAll() {
        return messages;
    }

    @Override
    public void save(Message object) {
        if(object.getId()==null) object.setId(nextId++);
        messages.add(object);
        logger.info("message has been add to history "+object);
    }

    @Override
    public void delete(Message object) {
        messages.remove(object);
        logger.info("message has been removed from history "+object);
    }

    @Override
    public Collection<Message> getLastMessages(int numberOfMessages) {
        if(messages.size()>numberOfMessages) {
            return messages.subList(messages.size() - numberOfMessages, messages.size());
        }
        return messages;
    }
}
