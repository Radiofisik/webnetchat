package netchat.model.dao;

import netchat.model.Impl.User;

public interface IUserDao extends GenericDao<User> {
    User getByLogin(String login);
}
