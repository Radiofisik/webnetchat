package netchat.model.dao;

import java.util.Collection;

public interface GenericDao<T> {
    T get(Integer id);
    Collection<T> getAll();
    void save(T object);
    void delete(T object);
}