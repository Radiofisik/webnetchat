package netchat.model.dao;

import netchat.model.Impl.Message;

import java.util.Collection;

public interface IMessageDao extends GenericDao<Message> {
    Collection<Message> getLastMessages(int numberOfMessages);
}
