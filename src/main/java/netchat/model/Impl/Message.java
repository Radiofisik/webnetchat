package netchat.model.Impl;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import netchat.model.IMessage;
import netchat.model.IUser;

import java.util.Date;
import java.util.Objects;

public class Message implements IMessage {
    private Integer id;
    private String content;
    private Date creationDate;
    @JsonDeserialize(as = User.class)
    private IUser user;

    @Override
    public String toString() {
        return content;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public boolean equals(Object obj) {
        return ((IMessage)obj).getId().equals(id);
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id=id;
    }

    @Override
    public String getContent() {
        return content;
    }

    @Override
    public void setContent(String content) {
        this.content=content;
    }

    @Override
    public IUser getUser() {
        return user;
    }

    @Override
    public void setUser(IUser user) {
        this.user= user;
    }

    @Override
    public Date getCreationDate() {
        return this.creationDate;
    }

    @Override
    public void setCreationDate(Date creationDate) {
        this.creationDate=creationDate;
    }
}
