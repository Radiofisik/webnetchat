package netchat.model.Impl;

import netchat.model.IUser;

public class User implements IUser {
    Integer id;
    private String login;

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public boolean equals(Object obj) {
        return ((IUser)obj).getId().equals(this.id);
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id=id;
    }

    @Override
    public String getLogin() {
        return login;
    }

    @Override
    public void setLogin(String login) {
        this.login=login;
    }
}
