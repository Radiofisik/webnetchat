package netchat.model;

import java.util.Date;

public interface IMessage {
    Integer getId();
    void  setId(Integer id);

    String getContent();
    void setContent(String content);

    IUser getUser();
    void setUser(IUser user);

    Date getCreationDate();
    void setCreationDate(Date creationDate);
}
