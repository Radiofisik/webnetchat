package netchat.model;

public interface IUser {
    Integer getId();
    void  setId(Integer id);

    String getLogin();
    void  setLogin(String login);
}
