package netchat.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.web.filter.CharacterEncodingFilter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Configuration
@EnableWebSecurity

public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        CharacterEncodingFilter filter = new CharacterEncodingFilter();
        filter.setEncoding("UTF-8");
        filter.setForceEncoding(true);
        http.addFilterBefore(filter,CsrfFilter.class);

        http.authorizeRequests()
                .antMatchers("/chat/**").authenticated()
                .anyRequest().permitAll()
                .and()
                .formLogin()
                    .successHandler(ajaxAuthSuccessHandler())
                    .failureHandler(ajaxAuthFailureHandler())
                    .usernameParameter("user")
                    .passwordParameter("pass")
                    .loginProcessingUrl("/j_spring_security_check")
                .and()
                .logout()
                    .logoutSuccessHandler(ajaxLogoutHandler());

        http.csrf().disable();
    }

    //allows to login any user with password as password
   @Autowired
    public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(new UserDetailsService() {
            @Override
            public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
                List<GrantedAuthority> authorityList=new ArrayList<GrantedAuthority>();
                UserDetails userDetails=new org.springframework.security.core.userdetails.User(s, "password",
                        true, true, true, true, authorityList);
                return userDetails;
            }
        });
    }

    @Bean
    SimpleUrlAuthenticationSuccessHandler ajaxAuthSuccessHandler(){
        return new SimpleUrlAuthenticationSuccessHandler(){
            @Override
            public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
                    response.getWriter().print("{\"success\":true}");
                    response.getWriter().flush();
            }
        };
    }

    @Bean
    SimpleUrlAuthenticationFailureHandler ajaxAuthFailureHandler(){
        return new SimpleUrlAuthenticationFailureHandler(){
            @Override
            public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
                    response.getWriter().print(
                            "{\"success\":false}");
                    response.getWriter().flush();
            }
        };
    }

    @Bean
    SimpleUrlLogoutSuccessHandler ajaxLogoutHandler(){
        return new SimpleUrlLogoutSuccessHandler(){
            @Override
            public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
                response.getWriter().print("{\"success\":true}");
            }
        };
    }
}
